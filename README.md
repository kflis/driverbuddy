# AVRA README

Roadmap:

1. Implement Settings

- including: custom commands, general settings that include changing responses, and service settings

2. Add a more complete command set

- including: restaurant/place search via yelp, buying goods through amazon (make sure to sign up for referral program for both)

3. Add notification response system

- including reading and replying to incoming texts (and other notifications) via voice and answering/rejecting
  phone calls via voice

4. Polish up things and fix any remaining bugs

- revamp gui--pretty ugly as it stands

5. Create ad-free (paid) and ad versions of the app and release!
