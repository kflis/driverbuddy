package com.kflis.test.driverbuddy;

/**
 * Holds static variables to be used in other parts of the application.
 */
public class AVRASettings {
    public static final int ROOT_HASH_INITIAL_SIZE = 16;
    public static final float ROOT_HASH_LOAD_FACTOR = .75f;
    public static final int CHILD_HASH_INITIAL_SIZE = 1;
    public static final float CHILD_HASH_LOAD_FACTOR = 1.01f;
    public static final int MAX_CHILD_TABLES = 3;
    public static final int RECOGNIZER_RESTART_TIME = 12500;
    public static String WAKE_UP_PHRASE = "wake";
    public static String WAKE_UP_GREETING = "say a command";
    public static String ERROR_PHRASE = "an error occurred while listening for your command";
    public static String ERROR_NO_SPEECH = "you woke me up for nothing!";
    public static int BUTTON_SIZE = 350;
    public static int RECOGNITION_PAUSE_TIME = 75;
    public static int HISTORY_SIZE = 64;
    public static int HISTORY_CONTAINER_MAX_TEXT_LENGTH = 250;
}
