package com.kflis.test.driverbuddy;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

public class SettingsFragment extends PreferenceFragment {
    Preference mExitPreference;
    AVRAActivity mAVRAActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings_fragment_layout);
    }

    @Override
    public void onStart() {
        super.onStart();
        setHasOptionsMenu(true);
        Activity activity = getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getActionBar();
            if (actionBar != null)
                actionBar.setDisplayHomeAsUpEnabled(true);
            activity.invalidateOptionsMenu();
        }
        mExitPreference = findPreference("exit_preference");
        if (mExitPreference != null) {
            mExitPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference preference) {
                    mAVRAActivity.mAVRAServiceConnected = false;
                    mAVRAActivity.stopService(new Intent(getActivity(), AVRAService.class));
                    mAVRAActivity.finish();
                    return true;
                }
            });
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mAVRAActivity = (AVRAActivity) activity;
        } catch (ClassCastException e) {
            mAVRAActivity = null;
            throw new ClassCastException(activity.toString()
                    + " must implement HomeFragmentCallback");
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                return homeFragmentTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean homeFragmentTransition() {
        Activity activity = getActivity();
        if (activity != null) {
            ActionBar actionBar = activity.getActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(false);
                actionBar.setHomeButtonEnabled(false);
            }
        }
        FragmentManager fm = getFragmentManager();
        if (fm != null)
            fm.popBackStack();
        return true;
    }
}
