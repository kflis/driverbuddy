package com.kflis.test.driverbuddy;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

//todo: change name to AVRA - Another Voice Recognition App -- ARVA: Mobile Assistant

/**
 * The main activity class that starts and binds to AVRAService after starting up ("service binding" allows AVRAActivity to
 * access to {@link com.kflis.test.driverbuddy.AVRAService} functions and "service starting" allows the service to stay active while the activity is inactive). <p></p>
 * After {@link AVRAActivity#onStart()} is called, the activity uses
 * {@link com.kflis.test.driverbuddy.AVRAService#bindAndStart(android.content.Context, android.content.ServiceConnection)}
 * to bind to and start {@link com.kflis.test.driverbuddy.AVRAService} and a {@link com.kflis.test.driverbuddy.PreloadFragment} is committed to the fragment manager. Then, once binded,
 * {@link com.kflis.test.driverbuddy.AVRAActivity#mAVRAServiceConnection} is created,
 * and the {@link com.kflis.test.driverbuddy.PreloadFragment} updates the user while {@link com.kflis.test.driverbuddy.AVRAPreload}
 * is is running. Once the preloading is finished, the {@link android.app.FragmentManager} transitions to {@link AVRAHomeFragment}
 * if nothing was on the FragmentManager's back stack (when there was no savedInstanceState upon activity creation in {@link com.kflis.test.driverbuddy.AVRAActivity#onCreate(android.os.Bundle)}),
 * otherwise it simply pops whatever was on its back stack using {@link android.app.FragmentManager#popBackStack()}
 */
public class AVRAActivity extends Activity implements AVRAHomeFragment.HomeFragmentCallback, PreloadFragment.PreloadFragmentCallback {

    private static final String TAG = "AVRAActivity";
    private AVRAService mAVRAService;
    protected boolean mAVRAServiceConnected = false;
    private AVRAHomeFragment mAVRAHomeFragment;
    private ButtonBitmaps mButtonBitmaps;
    private boolean mDisableBackButton = false; //used to disable the back button until PreloadFragment is removed

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate Called");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.avra_base_layout);
        Log.d(TAG, "saved instance state: " + (savedInstanceState == null? "null" : "not null") +
            "    service connection: " + mAVRAServiceConnected);


    }

    @Override
    protected void onStart() {
        Log.d(TAG, "onStart called");
        super.onStart();
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume called");
        super.onResume();
        mDisableBackButton = true;
        AVRAService.bindAndStart(this, mAVRAServiceConnection); //start and bind to AVRAService
        //display preload fragment (loading animation) then pop it off the back stack after the service has been bound (if there is another fragment in the back stack)
        FragmentManager fm = getFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof PreloadFragment && fm.getBackStackEntryCount() > 0)
            fm.popBackStack();
        FragmentTransaction ft = fm.beginTransaction().replace(R.id.fragment_container, new PreloadFragment());
        if (fragment != null && !(fragment instanceof PreloadFragment))
            ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    protected void onPostResume() {
        Log.d(TAG, "onPostResume called");
        super.onResume();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState Called");
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        Log.d(TAG, "onRestoreInstanceState Called");
        super.onRestoreInstanceState(savedInstanceState);

    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause Called");
        if (mAVRAServiceConnected) {
            mAVRAServiceConnected = false;
            mAVRAService.disconnectActivity();
            unbindService(mAVRAServiceConnection);
        }
        super.onPause();
    }

    @Override
    protected void onStop() {
        Log.d(TAG, "onStop Called");
        if (mAVRAServiceConnected) {
            mAVRAServiceConnected = false;
            mAVRAService.disconnectActivity();
            unbindService(mAVRAServiceConnection);
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy Called");
        if (mAVRAServiceConnected) {
            mAVRAServiceConnected = false;
            mAVRAService.disconnectActivity();
            unbindService(mAVRAServiceConnection);
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!mDisableBackButton) {
            FragmentManager fm = getFragmentManager();
            if (fm.getBackStackEntryCount() > 0) //to use the back button to go back without exiting the activity
                fm.popBackStack();
            else
                super.onBackPressed();
        }
    }

    /**
     * Used to bind the activity to {@link com.kflis.test.driverbuddy.AVRAService}
     */
    private ServiceConnection mAVRAServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "onServiceConnected Called");
            AVRAService.AVRABinder binder = (AVRAService.AVRABinder) iBinder;
            mAVRAService = binder.getService();
            mAVRAServiceConnected = true;
            mAVRAService.connectActivity();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mAVRAServiceConnected = false;
        }
    };



    @Override
    public AVRAService getService() {
        return mAVRAService;
    }

    @Override
    public ButtonBitmaps getBitmaps() {
        return mButtonBitmaps;
    }

    /**
     * Called by {@link com.kflis.test.driverbuddy.PreloadFragment} when it sees that {@link com.kflis.test.driverbuddy.AVRAPreload}
     * has finished loading.
     */
    @Override
    public void onFinishedLoading() {
        FragmentManager fm = getFragmentManager();
        if (fm.getBackStackEntryCount() > 0)
            fm.popBackStack();
        else {
            mAVRAHomeFragment = new AVRAHomeFragment();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out);
            ft.replace(R.id.fragment_container, mAVRAHomeFragment).commit();
        }
        mDisableBackButton = false;
    }

    @Override
    public boolean areBitmapsNull() {
        return mButtonBitmaps == null;
    }

    @Override
    public void setButtonBitmaps(ButtonBitmaps buttonBitmaps) {
        this.mButtonBitmaps = buttonBitmaps;
    }

    public static class ButtonBitmaps {
        private Bitmap micDisabled;
        private Bitmap micPressed;
        private Bitmap mic;


        public ButtonBitmaps(Bitmap mic, Bitmap micPressed, Bitmap micDisabled){
            this.micPressed = micPressed;
            this.micDisabled = micDisabled;
            this.mic = mic;
        }

        public Bitmap getMicPressed() { return micPressed; }
        public Bitmap getMicDisabled() { return micDisabled; }
        public Bitmap getMic() { return mic; }
    }

}
