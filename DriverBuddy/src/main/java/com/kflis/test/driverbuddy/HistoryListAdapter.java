package com.kflis.test.driverbuddy;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Custom {@link android.widget.ListView} adapter class that accomplishes three things: 1.
 * Holds a maximum number of elements as specified by {@link com.kflis.test.driverbuddy.AVRASettings#HISTORY_SIZE},
 * 2. Overwrites the oldest element when exceeding the history size, 3. Always pushes new elements to the
 * first slot in the array so that a {@link android.widget.ListView} displays the newest elements at the top.
 *
 * In addition, it uses two different row containers depending on the properties of elements added to the list.
 */
public class HistoryListAdapter extends BaseAdapter {

    private static final String TAG = "HistoryListAdapter";
    private int historySize;
    private HistoryContainer[] historyArray;
    private Activity context;
    private boolean notifyChange = false;
    private int count;

    public HistoryListAdapter(int size) {
        historySize = size;
        historyArray = new HistoryContainer[historySize];
    }

    public HistoryListAdapter refreshContext(Activity context) {
        this.context = context;
        return this;
    }

    public void add(HistoryContainer historyContainer) {
        if (count < historySize)
            ++count;
        int end = Math.min(historySize, count);
        for (int i = (end-1); i > 0; i--) //push back old elements to the end of the array
            historyArray[i] = historyArray[i-1];
        historyArray[0] = historyContainer; //place newest element in beginning
        if (notifyChange)
            notifyDataSetChanged();

    }



    public boolean remove(int position) {
        if (position > 0 && position < count) {
            int pos = position;
            for ( ; pos < count-1; pos++) //move elements after the deleted element forward toward the beginning of the array
                 historyArray[pos] = historyArray[pos+1];
            historyArray[--count] = null; //set the last element to null and update the element count
            if (notifyChange)
                notifyDataSetChanged();
            return true;
        }
        return false;
    }


    public void notifyDataSetOnChanges(boolean bool) {
        notifyChange = bool;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public Object getItem(int i) {
        return historyArray[i];
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View row = view;
        HistoryContainer historyContainer = historyArray[position]; //we need this to determine which row layout we'll use
        HistoryViewHolder holder = null;
        if (row == null) { //if the row is null, create it
           row = reloadRow(position);
        }
        else { //if the row isn't null, check if its position has changed, if it has, recreate the view for the row (since different rows can use different layouts)
            holder = (HistoryViewHolder) row.getTag();
            if (holder.position != position) {
                holder = null;
                row = reloadRow(position);
            }
        }

        if (holder == null)
            holder = (HistoryViewHolder) row.getTag();


        if (historyContainer.getIcon() == null)
            holder.icon.setImageResource(historyContainer.getIconId());
        else
            holder.icon.setImageDrawable(historyContainer.getIcon());
        holder.type.setText(historyContainer.getType());
        holder.time.setText(historyContainer.getTime());
        holder.text.setText(historyContainer.getText());

        return row;

    }

    private View reloadRow(int position) { //select the correct layout to use for the row and hold the data for each row view in a holder
        boolean useRightLayout = historyArray[position].isResponse();
        View row = context.getLayoutInflater().inflate(
                useRightLayout ? R.layout.list_view_container_left : R.layout.list_view_container_right, null);
        HistoryViewHolder holder = new HistoryViewHolder(
                (ImageView) row.findViewById(R.id.list_image),
                (TextView) row.findViewById(R.id.message_type),
                (TextView) row.findViewById(R.id.date),
                (TextView) row.findViewById(R.id.message),
                position
        );
        row.setTag(holder);
        return row;
    }

    private static class HistoryViewHolder {

        public ImageView icon;
        public TextView type;
        public TextView time;
        public TextView text;
        public int position;

        public HistoryViewHolder(ImageView icon, TextView type, TextView time, TextView text, int position) {
            this.icon = icon;
            this.type = type;
            this.time = time;
            this.text = text;
            this.position = position;
        }

    }
}
