package com.kflis.test.driverbuddy;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.media.AudioManager;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * The service responsible for handling transitions between states of the application and starting tasks, such as recognizing speech.
 */
public class AVRAService extends Service {

    public static final String TAG = "AVRAService";

    private static final int NOTIFICATION_ID = 1337101010; //1337 lololo
    private static final String STATE_FILE_NAME = "avrastate.dat";

    private AudioManager mAudioManager;
    private AVRARecognizer mAVRARecognizer;
    private AVRATextToSpeech mAVRATextToSpeech;
    private AVRAHomeFragment mAVRAHomeFragment;
    private HistoryListAdapter mHistoryListAdapter;
    private AVRACommandDispatcher mAVRACommandDispatcher;
    private boolean mProperShutdown = false;
    protected final AVRAState mAVRAState = new AVRAState();
    private AVRAPreload mAVRAPreload;

    private boolean mFragmentAttached = false;
    private boolean mActivityConnected = false;

    private NotificationManager mNotificationManager;

    private final IBinder mAVRABinder = new AVRABinder();

    /**
     * Starts AVRAService from the specified context, then binds the context to AVRAService
     * @param context where the service is being started/binded from
     * @param serviceConnection to bind the service to
     */
    public static void bindAndStart(Context context, ServiceConnection serviceConnection) {
        context.startService(new Intent(context, AVRAService.class));
        context.bindService(new Intent(context, AVRAService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAVRABinder;
    }

    @Override
    public void onCreate() {
        stopForeground(true);
        Process.setThreadPriority(Thread.MAX_PRIORITY);
        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        mAVRARecognizer = new AVRARecognizer(this);
        mHistoryListAdapter = new HistoryListAdapter(AVRASettings.HISTORY_SIZE);
        mAVRACommandDispatcher = new AVRACommandDispatcher(this);
        mNotificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        CommandList.attachService(this);
        HistoryContainer.setAdapter(mHistoryListAdapter);
        mAVRATextToSpeech = new AVRATextToSpeech(this);
        try {
            FileInputStream fis = openFileInput(STATE_FILE_NAME);
            byte [] prevState = new byte[4];
            fis.read(prevState);
            //mAVRAState.restoreState(ByteBuffer.wrap(prevState).order(ByteOrder.BIG_ENDIAN).getInt());
            fis.close();
            this.deleteFile(STATE_FILE_NAME);
        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } catch (Exception e) {
            this.deleteFile(STATE_FILE_NAME);
        }

        mAVRAPreload = new AVRAPreload(this);
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        mAVRARecognizer.destroy();
        mAVRATextToSpeech.destroy();
        stopForeground(true);
        this.deleteFile(STATE_FILE_NAME);
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    public class AVRABinder extends Binder {
        public AVRAService getService() {
            return AVRAService.this;
        }
    }

    /**
     * Create a new or update the current notification based on {@link com.kflis.test.driverbuddy.AVRAService#mAVRAState}
     * @param create specify true to create a new notification or false to notify an existing notification
     */
    private void showNotification(boolean create){

        Intent intent = new Intent(this, AVRAActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent =
                PendingIntent.getActivity(this, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Notification.Builder notification = new Notification.Builder(this)
                .setSmallIcon(android.R.drawable.btn_star_big_off)
                .setContentTitle("AVRA")
                .setOngoing(true)
                .setAutoCancel(false)
                .setContentIntent(contentIntent);

        String text = "AVRA is running!";

        if (mAVRAState.isActivatorOn()) {
            text = "AVRA is listening for \"" + AVRASettings.WAKE_UP_PHRASE + ".\"";
            notification.setSmallIcon(android.R.drawable.btn_star_big_on);
        }
        if (!mAVRAState.isReady()) {
            if (!mAVRAState.isAVRARecognizing())
                text = "AVRA is not listening.";
            if (mAVRAState.isAVRARecognizing()) {
                text = "AVRA is listening for a command...";
                notification.setTicker(text);
            }
            if (mAVRAState.isAVRASpeaking())
                text = "AVRA is speaking to you.";
        }

        if (create)
            startForeground(NOTIFICATION_ID, notification.setContentText(text).getNotification());
        else
            mNotificationManager.notify(NOTIFICATION_ID, notification.setContentText(text).getNotification());
    }

    /**
     * Transitions to the word activation "ready" state. <p></p>
     * Stays in {@link com.kflis.test.driverbuddy.AVRAService#mAVRARecognizer} to
     * try to listen for the wake-up phrase specified by {@link com.kflis.test.driverbuddy.AVRASettings#WAKE_UP_PHRASE}.
     * Once the phrase is heard, {@link com.kflis.test.driverbuddy.AVRAService#startCommandPrompt()} is called to prompt
     * the user to say a command.
     */
    public void startWordActivator() {
        mAVRAState.setReady();
        mAVRAState.setActivator();
        writeState();
        updateActivityUi();
        mAVRATextToSpeech.stop();
        mAVRARecognizer.destroy();
        mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, true);
        mAVRARecognizer.recognizeCodeWord();
    }

    /**
     * Transitions to the normal "ready" state.
     */
    public void stopWordActivator() {
        mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
        mAVRAState.reset();
        writeState();
        updateActivityUi();
        mAVRATextToSpeech.stop();
        mAVRARecognizer.destroy();
    }

    /**
     * Transitions to the state where the user is prompted to say a command. <p></p>
     * {@link com.kflis.test.driverbuddy.AVRAService#mAVRAState} is set to be not ready, and
     * {@link com.kflis.test.driverbuddy.AVRAService#mAVRATextToSpeech} is used to prompt the user
     * based on the phrase specified by {@link com.kflis.test.driverbuddy.AVRASettings#WAKE_UP_GREETING}.
     * After the TextToSpeech finishes speaking, {@link AVRAService#listenForCommand()} is called.
     */
    public void startCommandPrompt() {
        mAudioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
        mAVRATextToSpeech.stop();
        mAVRARecognizer.destroy();
        mAVRAState.setNotSpeaking();
        mAVRAState.setNotRecognizing();
        mAVRAState.setNotReady();
        updateActivityUi();
        mAVRATextToSpeech.askCommand();
    }

    /**
     * Transitions to the state command recognition state. <p></p>
     * If the speech was recognized successfully, {@link com.kflis.test.driverbuddy.AVRAService#dispatchCommand(java.util.ArrayList)}
     * is called to interpret the the command. If the speech was not recognized successfully,
     * {@link com.kflis.test.driverbuddy.AVRAService#listenError(int)} is called to report the error.
     */
    public void listenForCommand() {
        updateActivityUi();
        mAVRARecognizer.recognizeCommand();
    }

    /**
     * Transitions to the command dispatching state. <p></p>
     * Goes to {@link com.kflis.test.driverbuddy.AVRAService#mAVRACommandDispatcher} to try to translate the string into a command.
     * Whether or not a command gets successfully executed, {@link com.kflis.test.driverbuddy.AVRAService#listenResults(String)} is
     * called afterwards to report the results.
     * @param commandList list of {@link java.lang.String}s generated by {@link com.kflis.test.driverbuddy.AVRAService#mAVRARecognizer}
     */
    public void dispatchCommand (ArrayList<String> commandList) {
        mAVRAState.setNotReady();
        updateActivityUi();
        mAVRACommandDispatcher.dispatch(commandList);
    }

    /**
     * Transitions to the error reporting state. <p></p>
     * Reports the error to the user then calls {@link AVRAService#finishProcessing()} after the TextToSpeech finishes speaking.
     * @param error The error code returned from a {@link android.speech.SpeechRecognizer} object.
     */
    public void listenError(int error) {
        updateActivityUi();
        mAVRATextToSpeech.informError(error);
    }

    /**
     * Transitions to the result reporting state. <p></p>
     * Reports the results to the user then calls {@link AVRAService#finishProcessing()} after the TextToSpeech finishes speaking.
     * @param string results to be said
     */
    public void listenResults(String string) {
        updateActivityUi();
        mAVRATextToSpeech.informResults(string);
    }

    /**
     * Transitions to the final state to determine the following "ready" state. <p></p>
     * If {@link com.kflis.test.driverbuddy.AVRAService#mAVRAState} reports that the word activation is on,
     * {@link AVRAService#startWordActivator()} is called. Otherwise, {@link com.kflis.test.driverbuddy.AVRAService#mAVRAState}
     * is reset and the services goes into the normal "ready" state.
     */
    public void finishProcessing() {
        if (mAVRAState.isActivatorOn()) {
            mAVRAState.reset();
            startWordActivator();
        }
        else {
            mAVRAState.reset();
            updateActivityUi();
        }

    }

    /**
     * Simply inform the user of something that is not followed by a method call.
     * @param string what will be said
     */
    public void inform(String string) {
        mAVRATextToSpeech.inform(string);
    }

    public void attachFragment(AVRAHomeFragment avraHomeFragment) {
        this.mAVRAHomeFragment = avraHomeFragment;
        mFragmentAttached = true;
        mHistoryListAdapter.notifyDataSetOnChanges(true);
        stopForeground(true);
        updateActivityUi();

    }

    public void removeFragment() {
        mAVRAHomeFragment = null;
        mFragmentAttached = false;
        mHistoryListAdapter.notifyDataSetOnChanges(false);
    }

    public void connectActivity() {
        mActivityConnected = true;
        stopForeground(true);
    }

    public void disconnectActivity() {
        mActivityConnected = false;
        showNotification(true);
    }

    public HistoryListAdapter getListAdapter(Activity activity) {
        return mHistoryListAdapter.refreshContext(activity);
    }

    public void updateActivityUi() {
        Log.d(TAG, "updateActivityUI called");
        if (mFragmentAttached)
            mAVRAHomeFragment.update(mAVRAState);
        else
            if (!mActivityConnected)
                showNotification(false);
    }

    public boolean isTTSInitialized() {
        return mAVRATextToSpeech != null && mAVRATextToSpeech.isInitialized();
    }

    public AVRAPreload.PreloadStatus getStatus () {
        return mAVRAPreload.getStatus();
    }

    public void loadCommands(ArrayList<AVRACommandDispatcher.CommandAssociation> commandList) {
        if (mAVRACommandDispatcher != null)
            mAVRACommandDispatcher.populate(commandList);
    }

    public void writeState() {
        FileOutputStream fos = null;
        try {
            fos = openFileOutput(STATE_FILE_NAME, Context.MODE_PRIVATE);
            byte [] bytes = ByteBuffer.allocate(4).putInt(mAVRAState.getState()).array();
            fos.write(bytes);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class AVRAState {

        private int state = 0x0000F000; //ready state

        public boolean isActivatorOn() {
            return (0x0000000F & state) == 0x0000000F;
        }

        public void setNotActivator() {
            state = (0xFFFFFFF0 & state);
        }

        public void setActivator() {
            state = (0x0000000F | state);
        }

        public boolean isAVRARecognizing() {
            return (0x000000F0 & state) == 0x000000F0;
        }

        public void setNotRecognizing() {
            state = (0xFFFFFF0F & state);
        }

        public void setRecognizing() {
            setNotSpeaking();
            state = (0x000000F0 | state);
        }

        public boolean isAVRASpeaking() {
            return (0x00000F00 & state) == 0x00000F00;
        }

        public void setNotSpeaking() {
            state = (0xFFFFF0FF & state);
        }

        public void setSpeaking() {
            setNotRecognizing();
            state = (0x00000F00 | state);
        }

        public boolean isReady() {
            return (0x0000F000 & state) == 0x0000F000;
        }

        public void setNotReady() {
            state = (0xFFFF0FFF & state);
        }

        public void setReady() {
            state = (0x0000F000 | state);
        }

        public boolean isProcessing() {
            return (0x000F0000 & state) == 0x00F0000;
        }

        public void setNotProcessing(){
            state = (0xFFF0FFFF & state);
        }

        public void setProcessing() {
            state = (0x000F0000 | state);
        }

        public void reset() { //reset to ready state
            state = 0x0000F000;
        }

        public int getState() {
            return state & 0x0000000F;
        }

        public void restoreState(int state) {
            this.state = state;
        }



    }
}
