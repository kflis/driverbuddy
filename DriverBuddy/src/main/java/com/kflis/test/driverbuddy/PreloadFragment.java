package com.kflis.test.driverbuddy;

import android.app.Activity;
import android.app.Fragment;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * This fragment is displayed whenever {@link AVRAActivity#onStart()} is called. Displays the preloading
 * status then calls {@link AVRAActivity#onFinishedLoading()} after {@link AVRAPreload#getStatus()} reports
 * that preloading is finished.
 */
public class PreloadFragment extends Fragment {

    private static final String TAG = "PreloadFragment";
    private PreloadFragmentCallback mCallback;
    private AsyncTask mPreloadTask;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView called");
        return inflater.inflate(R.layout.preload_layout, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (PreloadFragmentCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement PreloadFragmentCallback");

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart called");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPreloadTask != null)
            mPreloadTask.cancel(true);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mPreloadTask != null)
            mPreloadTask.cancel(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
        mPreloadTask = new AsyncTask<Void,Integer,Void>() {

            View view = getView();
            TextView textView;
            TextView progView;

            {
                if (view != null) {
                    textView = (TextView) view.findViewById(R.id.loading_text);
                    progView = (TextView) view.findViewById(R.id.progress_text);
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                if (mCallback.areBitmapsNull()) {
                    Bitmap mic = decodeSampledBitmapFromResource(getResources(), R.drawable.mic_not_pressed, AVRASettings.BUTTON_SIZE, AVRASettings.BUTTON_SIZE);
                    Bitmap micPressed = decodeSampledBitmapFromResource(getResources(), R.drawable.mic_pressed, AVRASettings.BUTTON_SIZE, AVRASettings.BUTTON_SIZE);
                    Bitmap disabled = decodeSampledBitmapFromResource(getResources(), R.drawable.mic_disabled, AVRASettings.BUTTON_SIZE, AVRASettings.BUTTON_SIZE);
                    mCallback.setButtonBitmaps(new AVRAActivity.ButtonBitmaps(mic, micPressed, disabled));
                }

                AVRAService avraService = mCallback.getService();
                while (avraService == null) {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    avraService = mCallback.getService();
                }
                AVRAPreload.PreloadStatus status = avraService.getStatus();
                while (!status.done) {
                    publishProgress(status.resource, status.perc);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    status = avraService.getStatus();
                }


                return null;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                if (textView != null && progView != null) {
                    textView.setText(values[0]);
                    progView.setVisibility(View.VISIBLE);
                    progView.setText(String.valueOf(values[1]) + "%");
                }

            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mCallback.onFinishedLoading();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPreloadTask != null)
            mPreloadTask.cancel(true);
    }

    //shameless copypaste from android bitmap tutorial
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    public interface PreloadFragmentCallback {
        public void onFinishedLoading();
        public void setButtonBitmaps(AVRAActivity.ButtonBitmaps buttonBitmaps);
        public boolean areBitmapsNull();
        public AVRAService getService();
    }
}
