package com.kflis.test.driverbuddy;


import android.media.AudioManager;
import android.os.AsyncTask;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

/**
 * This class uses a TextToSpeech engine to communicate to the user. If the type of communication involves
 * a state transition, the transition call is performed in {@link com.kflis.test.driverbuddy.AVRATextToSpeech#onDone(String)},
 * after the engine finishes speaking.
 */
public class AVRATextToSpeech extends UtteranceProgressListener implements TextToSpeech.OnInitListener{
    private TextToSpeech mTextToSpeech;
    private static final String TAG = "AVRATextToSpeech";
    private HashMap<String, String> mHashMap = new HashMap<String, String>();
    private boolean mInitialized = false;
    private AVRAService mAVRAService;

    AVRATextToSpeech(AVRAService avraService) {
        //tps = new ThreadPoolExecutor("tps");

        this.mAVRAService = avraService;
        mTextToSpeech = new TextToSpeech(this.mAVRAService, this);
        //mTextToSpeech.setOnUtteranceProgressListener(this);
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, TAG);
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_STREAM, String.valueOf(AudioManager.STREAM_MUSIC));
    }

    /**
     * Speaks the {@link com.kflis.test.driverbuddy.AVRASettings#WAKE_UP_GREETING} then performs a state transition
     * to {@link AVRAService#listenForCommand()}
     */
    public void askCommand() {
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "command");
        say(AVRASettings.WAKE_UP_GREETING);
    }

    /**
     * Speaks a phrase based on the error occurrence during speech recognition then performs a state transition
     * to {@link AVRAService#finishProcessing()}
     * @param errorCode the {@link android.speech.SpeechRecognizer} error code
     */
    public void informError(int errorCode) {
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "error");
        if (errorCode == SpeechRecognizer.ERROR_SPEECH_TIMEOUT)
            say(AVRASettings.ERROR_NO_SPEECH);
        else
            say(AVRASettings.ERROR_PHRASE);
    }

    /**
     * Speaks a phrase based on the results of a speech recognition then performs a state transition
     * to {@link AVRAService#finishProcessing()}
     * @param string the phrase to be spoken
     */
    public void informResults(String string) {
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "results");
        String s = "Error";
        if (string != null)
            s = string;
        say(s);
    }

    /**
     * Speaks a phrase with no method calls afterwards.
     * @param string the phrase to be spoken
     */
    public void inform(String string) {
        mHashMap.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "other");
        String s = "Error";
        if (string != null)
            s = string;
        say(s);
    }

    /**
     * State speaking using the specified TextToSpeech engine and {@link com.kflis.test.driverbuddy.AVRATextToSpeech#mHashMap} parameters.
     * And creates a {@link com.kflis.test.driverbuddy.HistoryContainer} update.
     * @param phrase the phrase to be spoken
     */
    private void say(String phrase) {
        mTextToSpeech.speak(phrase, TextToSpeech.QUEUE_FLUSH, mHashMap);
        HistoryContainer.createHistoryContainer()
                .setIsResponse(true)
                .setIconId(android.R.drawable.stat_notify_chat)
                .setType(HistoryContainer.TYPE_RESPONSE)
                .setText("\"" + phrase + "\"")
                .sendToAdapter();
    }

    public void stop() {
        mTextToSpeech.stop();
    }

    public void destroy() {
        stop();
        mTextToSpeech.shutdown();
    }

    public boolean isInitialized() {
        return mInitialized;
    }

    @Override
    public void onInit(int i) {
        Log.d(TAG, "onInit called");
        if (i == TextToSpeech.SUCCESS)
        {
            mTextToSpeech.setOnUtteranceProgressListener(this);
            int result = mTextToSpeech.setLanguage(Locale.US);
            mInitialized = true;

            if (mAVRAService.mAVRAState.isActivatorOn()) {
                mAVRAService.startWordActivator();
            }

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.d(TAG, "This Language is not supported");
            }
        } else {
            Log.d(TAG, "Initilization Failed!");
        }
    }

    @Override
    public void onStart(String s) {

        Log.d(TAG, "onStart called");

        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mAVRAService.mAVRAState.setSpeaking();
                mAVRAService.updateActivityUi();
            }
        }.execute(null,null,null);
    }

    @Override
    public void onDone(final String s) {

        Log.d(TAG, "onDone called");

        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mAVRAService.mAVRAState.setNotSpeaking();
                mAVRAService.updateActivityUi();
                if (s.equals("command"))
                    mAVRAService.listenForCommand();
                else
                    if(s.equals("error") || s.equals("results"))
                        mAVRAService.finishProcessing();
            }
        }.execute(null,null,null);
    }

    @Override
    public void onError(String s) {
        Log.d(TAG, "onError called");
        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mAVRAService.mAVRAState.setNotSpeaking();
                mAVRAService.updateActivityUi();
            }
        }.execute(null,null,null);
    }

}
