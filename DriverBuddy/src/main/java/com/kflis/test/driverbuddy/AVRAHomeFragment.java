package com.kflis.test.driverbuddy;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * This Fragment displays the main activity GUI of AVRA.
 */
public class AVRAHomeFragment extends Fragment {

    private TextView mStatusText;
    private ImageButton mActionButton;
    private ImageView mDisabledImage;
    private ListView mListView;
    private ProgressBar mProcessingProgressBar;
    private TextView mProcessingTextView;
    private HomeFragmentCallback mHomeFragmentCallback;
    private boolean mActivatorEnabled = false;
    private static final String TAG = "AVRAHomeFragment";

    @Override
    public void onAttach(Activity activity) {

        try {
            mHomeFragmentCallback = (AVRAActivity) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement HomeFragmentCallback");

        }
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.avra_fragment_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(TAG, "onActivityCreated Called");
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        Log.d(TAG, "onStart Called");
        super.onStart();
        setHasOptionsMenu(true);
        final Activity activity = getActivity();
        final View view = getView();

        if (activity != null && view != null && mHomeFragmentCallback.getService() != null) {
            mStatusText = (TextView) view.findViewById(R.id.status_text);
            mProcessingProgressBar = (ProgressBar) view.findViewById(R.id.processing_results_progress);
            mProcessingTextView = (TextView) view.findViewById(R.id.processing_results_text);

            mListView = (ListView) view.findViewById(R.id.history_list_view);
            if (mListView != null)
                mListView.setAdapter(mHomeFragmentCallback.getService().getListAdapter(getActivity()));

            ActionBar actionBar = activity.getActionBar();
            if (actionBar != null)
                actionBar.setDisplayHomeAsUpEnabled(false);
            activity.invalidateOptionsMenu();

            mDisabledImage = (ImageView) view.findViewById(R.id.button_disabled_image);
            if (mDisabledImage != null) {
                mDisabledImage.setImageBitmap(mHomeFragmentCallback.getBitmaps().getMicDisabled());
                mDisabledImage.setVisibility(View.GONE);
            }

            BitmapDrawable mic = new BitmapDrawable(getResources(), mHomeFragmentCallback.getBitmaps().getMic());
            BitmapDrawable micPressed = new BitmapDrawable(getResources(), mHomeFragmentCallback.getBitmaps().getMicPressed());

            StateListDrawable states = null;
            if (mic != null && micPressed != null) {
                states = new StateListDrawable();
                states.addState(new int[] {android.R.attr.state_pressed},
                        micPressed);
                states.addState(new int[] {android.R.attr.state_focused},
                        mic);
                states.addState(new int[] { },
                        mic);
            }
            mActionButton = (ImageButton) view.findViewById(R.id.action_button);

            if (mActionButton != null && states != null) {
                mActionButton.setImageDrawable(states);

                mActionButton.setOnClickListener(new Button.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        mActionButton.setEnabled(false);
                        HistoryContainer.createHistoryContainer()
                                .setIsResponse(false)
                                .setIconId(android.R.drawable.ic_menu_info_details)
                                .setType(HistoryContainer.TYPE_ACTION)
                                .setText("You activated AVRA!")
                                .sendToAdapter();
                        mHomeFragmentCallback.getService().startCommandPrompt();

                    }
                });

                mActionButton.setOnLongClickListener(new ImageButton.OnLongClickListener() {

                    @Override
                    public boolean onLongClick(View view) {
                        mActionButton.setEnabled(false);
                        AVRAService avraService = mHomeFragmentCallback.getService();
                        HistoryContainer history =
                                HistoryContainer.createHistoryContainer()
                                        .setIsResponse(false)
                                        .setIconId(android.R.drawable.ic_menu_info_details)
                                        .setType(HistoryContainer.TYPE_ACTION);
                        if (mActivatorEnabled) {
                            avraService.stopWordActivator();
                            history.setText("Word activation disabled!");
                        } else {
                            avraService.startWordActivator();
                            history.setText("Word activation enabled! Say \"" +
                                    AVRASettings.WAKE_UP_PHRASE + "\" to wake up AVRA");
                        }
                        history.sendToAdapter();
                        return true;
                    }
                });
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume Called");
        if (mHomeFragmentCallback.getService() != null)
            mHomeFragmentCallback.getService().attachFragment(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        AVRAService avraService = mHomeFragmentCallback.getService();
        if (avraService != null)
            avraService.removeFragment();
        Log.d(TAG, "onPause Called");
    }

    @Override
    public void onStop() {
        super.onStop();
        AVRAService avraService = mHomeFragmentCallback.getService();
        if (avraService != null)
            avraService.removeFragment();
        Log.d(TAG, "onStop Called");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AVRAService avraService = mHomeFragmentCallback.getService();
        if (avraService != null)
            avraService.removeFragment();
        Log.d(TAG, "onDestroyView Called");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AVRAService avraService = mHomeFragmentCallback.getService();
        if (avraService != null)
            avraService.removeFragment();
        Log.d(TAG, "onDestroy Called");
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.avra_activity_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.settings_menu_button:
                return settingsTransition();
        }
        return super.onOptionsItemSelected(item);
    }

    private boolean settingsTransition() {
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            fm.beginTransaction()
                    .replace(R.id.fragment_container, new SettingsFragment(), "Settings")
                    .addToBackStack(null)
                    .commit();
            return true;
        }
        return false;
    }

    /**
     * Updates {@link AVRAHomeFragment} with the data from the current state of the {@link com.kflis.test.driverbuddy.AVRAService}.
     * Always call update from {@link com.kflis.test.driverbuddy.AVRAService} after a state change while this fragment is attached to the service
     * and when the fragment is being attached to the service--to ensure that the fragment UI is updated with
     * the latest information.
     *
     * @param state The state variable to check.
     */
    public void update(AVRAService.AVRAState state) {
        if (state.isAVRARecognizing()) {
            mStatusText.setText(R.string.listening);
        }
        else {
            mStatusText.setText(R.string.not_listening);
        }

        if (state.isActivatorOn()) {
            mActivatorEnabled = true;
            mActionButton.setBackgroundDrawable(getResources().getDrawable(android.R.color.holo_green_light));
        }
        else {
            mActivatorEnabled = false;
            mActionButton.setBackgroundDrawable(null);
        }

        //this confusing if else works since isAVRASpeaking and isAVRARecognizing cannot both be true
        if (state.isAVRASpeaking())
            mStatusText.setText(R.string.speaking);
        else
            if (!state.isAVRARecognizing())
                mStatusText.setText(R.string.not_listening);

        if (state.isReady()) {
            mActionButton.setEnabled(true);
            mDisabledImage.setVisibility(View.GONE);
        }
        else {
            mActionButton.setEnabled(false);
            mDisabledImage.setVisibility(View.VISIBLE);
        }

        if (state.isProcessing()) {
            if (mProcessingTextView != null && mProcessingProgressBar != null) {
                mProcessingProgressBar.setVisibility(View.VISIBLE);
                mProcessingTextView.setVisibility(View.VISIBLE);
            }
        }
        else {
            if (mProcessingTextView != null && mProcessingProgressBar != null) {
                mProcessingProgressBar.setVisibility(View.GONE);
                mProcessingTextView.setVisibility(View.GONE);
            }
        }


    }

    public interface HomeFragmentCallback {

        public AVRAService getService();
        public AVRAActivity.ButtonBitmaps getBitmaps();

    }


}
