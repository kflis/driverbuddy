package com.kflis.test.driverbuddy;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import java.util.ArrayList;


/**
 * This class uses {@link android.speech.SpeechRecognizer} to detect activation phrases or return
 * speech-to-text strings generated by the microphone input.
 */
public class AVRARecognizer {

    private SpeechRecognizer mSpeechRecognizer = null;
    private AVRAService mAVRAService;
    private CommandListener mCommandListener;
    private CodeWordListener mCodeWordListener;
    private RestartTask mRestartTask;
    //private ExecutorService executor = Executors.newSingleThreadScheduledExecutor()


    public AVRARecognizer(AVRAService avraService) {
        this.mAVRAService = avraService;
        //mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(this.mAVRAService);
        mCommandListener = new CommandListener();
        mCodeWordListener = new CodeWordListener();



    }

    /**
     * Keep listening to speech input until the activation phrase given by {@link com.kflis.test.driverbuddy.AVRASettings#WAKE_UP_PHRASE}
     * then call {@link AVRAService#startCommandPrompt()} to prompt the user that they have woken up the service.
     */
    public void recognizeCodeWord() {
        mCodeWordListener.resetFound();
        mRestartTask = new RestartTask(mCodeWordListener);
        mRestartTask.execute(null, null, null);
    }

    /**
     * Generate a string from the microphone that is sent to {@link com.kflis.test.driverbuddy.AVRAService#dispatchCommand(java.util.ArrayList)} )}
     * that will be parsed to possibly start a command. If there is an error in the input, {@link com.kflis.test.driverbuddy.AVRAService#listenError(int)}
     * is called with the error code given by {@link android.speech.SpeechRecognizer#ERROR_AUDIO}, {@link android.speech.SpeechRecognizer#ERROR_CLIENT},
     * {@link android.speech.SpeechRecognizer#ERROR_INSUFFICIENT_PERMISSIONS}, {@link android.speech.SpeechRecognizer#ERROR_NETWORK},
     * {@link android.speech.SpeechRecognizer#ERROR_NETWORK_TIMEOUT}, {@link android.speech.SpeechRecognizer#ERROR_NO_MATCH},
     * {@link android.speech.SpeechRecognizer#ERROR_RECOGNIZER_BUSY}, {@link android.speech.SpeechRecognizer#ERROR_SERVER}, or
     * {@link android.speech.SpeechRecognizer#ERROR_SPEECH_TIMEOUT}
     */
    public void recognizeCommand() {
        mRestartTask = new RestartTask(mCommandListener);
        mRestartTask.execute(null, null, null);
    }

    /**
     * Safely destroy the speech recognizer. Repeated calls simply do nothing.
     */
    public void destroy() {
        if (mSpeechRecognizer != null) {
            mSpeechRecognizer.cancel();
            mSpeechRecognizer.destroy();
            mSpeechRecognizer = null;
        }

        if (mRestartTask != null) {
            mRestartTask.stop();
            mRestartTask = null;
        }
    }


    /**
     * This class is used for recognizing wake-up phrases. After {@link com.kflis.test.driverbuddy.AVRARecognizer.CodeWordListener#onResults(android.os.Bundle)}
     * or {@link com.kflis.test.driverbuddy.AVRARecognizer.CodeWordListener#onError(int)} is called, if the wake-up phrase isn't found, the recognizer will continue
     * until canceled or until the wake-up phrase is detected. Once the wake-up phrase is detected, {@link AVRAService#listenForCommand()} will be called.
     */
    private class CodeWordListener implements RecognitionListener, AVRARecognitionCallback {

        private static final String TAG = "AVRARecognizer.CodeWordListener";
        private boolean mFound;

        public void resetFound() { mFound = false; }

        public void recognizeSpeech() {
            Intent recognizeIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 3);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"com.kflis.test.driverbuddy");
            Log.d(TAG, "in recognizeCodeWord, starting to listen");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
            if (mSpeechRecognizer == null) {
                mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mAVRAService);
            }
            mSpeechRecognizer.setRecognitionListener(this);
            mSpeechRecognizer.startListening(recognizeIntent);

        }

        private boolean parseResults(Bundle bundle) {
            if (bundle != null) {
                ArrayList<String> resultsArray = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (resultsArray != null) {
                    for (String s : resultsArray) {
                        Log.d(TAG, "result: " + s.toLowerCase());
                        if (s.toLowerCase().contains(AVRASettings.WAKE_UP_PHRASE)) {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private void sendHistoryUpdate() {
            HistoryContainer.createHistoryContainer()
                    .setIsResponse(false)
                    .setIconId(android.R.drawable.ic_menu_info_details)
                    .setType(HistoryContainer.TYPE_ACTION)
                    .setText("You woke up AVRA!")
                    .sendToAdapter();
        }

        @Override
        public void onReadyForSpeech(Bundle bundle) {
            Log.d(TAG, "in onReadyForSpeech");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
            mAVRAService.mAVRAState.setRecognizing();
            mAVRAService.updateActivityUi();
        }

        @Override
        public void onBeginningOfSpeech() {
            Log.d(TAG, "in onBeginningOfSpeech");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onRmsChanged(float v) {
            //Log.d(TAG, "in onRmsChanged");
        }

        @Override
        public void onBufferReceived(byte[] bytes) {
            Log.d(TAG, "in onBufferReceived");
        }

        @Override
        public void onEndOfSpeech() {
            Log.d(TAG, "in onEndOfSpeech");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onError(final int error) {
            Log.d(TAG, "in onError");

            if (mRestartTask != null)
                mRestartTask.stop();
            mAVRAService.mAVRAState.setNotRecognizing();
            mAVRAService.updateActivityUi();
            mRestartTask = new RestartTask(this);
            mRestartTask.execute(null, null, null);

        }

        @Override
        public void onResults(Bundle bundle) {
            Log.d(TAG, "in onResults");

            mAVRAService.mAVRAState.setNotRecognizing();
            mAVRAService.updateActivityUi();
            if (parseResults(bundle)) {
                if (!mFound) {
                    mFound = true;
                    sendHistoryUpdate();
                    destroy();
                    mAVRAService.startCommandPrompt();
                }
            }
            else {
                if (mRestartTask != null)
                    mRestartTask.stop();
                mRestartTask = new RestartTask(this);
                mRestartTask.execute(null, null, null);
            }


        }

        @Override
        public void onPartialResults(Bundle bundle) {
            Log.d(TAG, "in onPartialResults");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
            if (parseResults(bundle)) {
                if (!mFound) {
                    mFound = true;
                    sendHistoryUpdate();
                    destroy();
                    mAVRAService.startCommandPrompt();
                }
            }
        }

        @Override
        public void onEvent(int i, Bundle bundle) {
            Log.d(TAG, "in onEvent");
        }
    }

    /**
     * This class is used for recognizing commands. Once {@link com.kflis.test.driverbuddy.AVRARecognizer.CommandListener#recognizeSpeech()}
     * is called, {@link com.kflis.test.driverbuddy.AVRARecognizer.CommandListener#onError(int)} or
     * {@link com.kflis.test.driverbuddy.AVRARecognizer.CommandListener#onResults(android.os.Bundle)} will be called to report the error or parse the results
     * using {@link com.kflis.test.driverbuddy.AVRAService#listenError(int)} or {@link com.kflis.test.driverbuddy.AVRAService#dispatchCommand(java.util.ArrayList)},
     * respectively.
     */
    private class CommandListener implements RecognitionListener, AVRARecognitionCallback {

        private static final String TAG = "AVRARecognizer.CommandListener";

        public void recognizeSpeech() {
            Intent recognizeIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 5);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
            recognizeIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,"com.kflis.test.driverbuddy");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
            Log.d(TAG, "in recognizeCodeWord, starting to listen");
            if (mSpeechRecognizer == null) {
                mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mAVRAService);
            }
            mSpeechRecognizer.setRecognitionListener(this);
            mSpeechRecognizer.startListening(recognizeIntent);
        }

        @Override
        public void onReadyForSpeech(Bundle bundle) {
            Log.d(TAG, "in onReadyForSpeech");

            mAVRAService.mAVRAState.setRecognizing();
            mAVRAService.updateActivityUi();
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onBeginningOfSpeech() {
            Log.d(TAG, "in onBeginningOfSpeech");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onRmsChanged(float v) {
            //Log.d(TAG, "in onRmsChanged");
        }

        @Override
        public void onBufferReceived(byte[] bytes) {
            Log.d(TAG, "in onBufferReceived");
        }

        @Override
        public void onEndOfSpeech() {
            Log.d(TAG, "in onEndOfSpeech");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onError(final int error) {
            Log.d(TAG, "in onError");

            destroy();
            mAVRAService.mAVRAState.setNotRecognizing();
            mAVRAService.updateActivityUi();
            mAVRAService.listenError(error);

        }

        @Override
        public void onResults(final Bundle bundle) {
            Log.d(TAG, "in onResults");

            destroy();
            mAVRAService.mAVRAState.setNotRecognizing();
            mAVRAService.updateActivityUi();
            ArrayList<String> strings = bundle.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
            mAVRAService.dispatchCommand(strings);

        }

        @Override
        public void onPartialResults(Bundle bundle) {
            Log.d(TAG, "in onPartialResults");
            if (mRestartTask != null)
                mRestartTask.restartTimer();
        }

        @Override
        public void onEvent(int i, Bundle bundle) {
            Log.d(TAG, "in onEvent");
        }
    }


    /**
     * Used to restart the speech recognizer if it has not responded in the time specified
     * by {@link com.kflis.test.driverbuddy.AVRASettings#RECOGNIZER_RESTART_TIME}
     */
    private final class RestartTask extends AsyncTask<Void, Integer, Void> {

        private final AVRARecognitionCallback mCallback;
        private final Object mLock = new Object();
        private boolean mFinished;
        private NotRespondingTimer mTimer = new NotRespondingTimer(AVRASettings.RECOGNIZER_RESTART_TIME, AVRASettings.RECOGNIZER_RESTART_TIME);

        public RestartTask(AVRARecognitionCallback callback) {
            this.mCallback = callback;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                while (true) {
                    setFinished(false);
                    Thread.sleep(AVRASettings.RECOGNITION_PAUSE_TIME);
                    if (!Thread.interrupted())
                        publishProgress();
                    while (!isFinished() && !Thread.interrupted()) {
                        Thread.sleep(100);
                    }
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (!Thread.interrupted())
                if (values.length == 0)
                    mCallback.recognizeSpeech();
                else {
                    mSpeechRecognizer.cancel();
                    mSpeechRecognizer.destroy();
                    mSpeechRecognizer = SpeechRecognizer.createSpeechRecognizer(mAVRAService);
                    mAVRAService.inform("Recognizer not responding. Restarting.");
                    restartTimer();
                }
        }

        public void stop() {
            mTimer.cancel();
            mTimer = null;
            cancel(true);
        }

        public void restartTimer() {
            synchronized (mLock) {
                mTimer.cancel();
                mTimer = null;
                mTimer = new NotRespondingTimer(12500, 12500);
                mTimer.start();
            }
        }

        private boolean isFinished() {
            synchronized (mLock) {
                return mFinished;
            }
        }

        private void setFinished(boolean value) {
            synchronized (mLock) {
                mFinished = value;
            }
        }


        private final class NotRespondingTimer extends CountDownTimer {

            public NotRespondingTimer(long millisInFuture, long countDownInterval) {
                super(millisInFuture, countDownInterval);
            }

            @Override
            public void onTick(long l) {
                //don't care
            }

            @Override
            public void onFinish() {
                publishProgress(1);
                setFinished(true);
            }

        }

    }


    private interface AVRARecognitionCallback {
        public void recognizeSpeech();
    }

}
