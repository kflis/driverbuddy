package com.kflis.test.driverbuddy;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CommandList {

    public static final String NAVIGATE = "CMD.NAVIGATE";
    public static final String OPEN_APP = "CMD.OPEN_APP";
    public static final String QUESTION = "CMD.QUESTION";
    public static final String REPEAT = "CMD.REPEAT";
    public static final String SHORTCUT = "CMD.SHORTCUT";
    public static final String START_WORD_ACTIVATOR = "CMD.START_WORD_ACTIVATOR";
    public static final String STOP_WORD_ACTIVATOR = "CMD.STOP_WORD_ACTIVATOR";
    public static final String WIFI_OFF = "CMD.WIFI_OFF";
    public static final String WIFI_ON = "CMD.WIFI_ON";

    private static final String GOOGLE_API_KEY = "AIzaSyDrPxpEc-qo4BDhjU-_lVi7nLMWAL4OHNI";

    public static final boolean USES_PARAMS = true;
    public static final boolean DOES_NOT_USE_PARAMS = false;

    public static ArrayList<ActivityData> sActivityData = null;


    /**
     * The {@link java.util.HashMap} that maps phrases encoded in {@link java.lang.String} to
     * {@link com.kflis.test.driverbuddy.CommandList.AVRAShortcut}.
     *
     * Every key in sShortcutMap should also have a
     * {@link com.kflis.test.driverbuddy.AVRACommandDispatcher.CommandAssociation} linked to
     * {@link com.kflis.test.driverbuddy.CommandList#SHORTCUT} in order for the shortcut to be able
     * to be searched for along with every other command.
     */
    public static HashMap<String, AVRAShortcut> sShortcutMap = new HashMap<String, AVRAShortcut>(4, 1.0f);

    private static AVRAService mAVRAService = null;

    public static void attachService(AVRAService service) {
        mAVRAService = service;
    }

    /**
     * Maps command identifier {@link java.lang.String}s to {@link com.kflis.test.driverbuddy.AVRACommand}s
     */
    public static HashMap<String, AVRACommand> sStringToCommandMap = new HashMap<String, AVRACommand>(4, 1.0f);

    static {
        sStringToCommandMap.put(NAVIGATE, new AVRACommand(USES_PARAMS) {
            @Override
            public void begin(String command, String params) {
                navigate(command, params);
            }
        });
        sStringToCommandMap.put(OPEN_APP, new AVRACommand(USES_PARAMS) {
            @Override
            public void begin(String command, String params) {
                openApp(command, params);
            }
        });

        sStringToCommandMap.put(QUESTION, new AVRACommand(USES_PARAMS) {
            @Override
            public void begin(String command, String params) {
                question(command, params);
            }
        });

        sStringToCommandMap.put(REPEAT, new AVRACommand(USES_PARAMS) {
            @Override
            public void begin(String command, String params) {
                repeat(command, params);
            }
        });

        sStringToCommandMap.put(SHORTCUT, new AVRACommand(DOES_NOT_USE_PARAMS) {
            @Override
            public void begin(String command, String params) {
                shortcut(command, params);
            }
        });

        sStringToCommandMap.put(START_WORD_ACTIVATOR, new AVRACommand(DOES_NOT_USE_PARAMS) {
            @Override
            public void begin(String command, String params) {
                startWordActivator(command, params);
            }
        });

        sStringToCommandMap.put(STOP_WORD_ACTIVATOR, new AVRACommand(DOES_NOT_USE_PARAMS) {
            @Override
            public void begin(String command, String params) {
                stopWordActivator(command, params);
            }
        });

        sStringToCommandMap.put(WIFI_OFF, new AVRACommand(DOES_NOT_USE_PARAMS) {
            @Override
            public void begin(String command, String params) {
                wifiOff(command, params);
            }
        });

        sStringToCommandMap.put(WIFI_ON, new AVRACommand(DOES_NOT_USE_PARAMS) {
            @Override
            public void begin(String command, String params) {
                wifiOn(command, params);
            }
        });
    }

    /**
     * Get the command associated with a String.
     * @param cmdId The string value of the command.
     * @return the {@link com.kflis.test.driverbuddy.AVRACommand} associated with the String or null if nothing exists
     */
    public static AVRACommand getCommand(String cmdId) {
        return sStringToCommandMap.get(cmdId);
    }

    /**
     * Creates a new intent to start navigation based on the params, then starts the activity.
     *
     * @param command the command that called this method
     * @param params the parameters associated with the command
     */
    public static void navigate(String command, String params) {
        if (params.length() == 0) {
            mAVRAService.listenResults("I knew a guy who told me to just navigate. He ended up in a river.");
        }
        else {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q="+ params));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                    Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
            mAVRAService.startActivity(intent);
            mAVRAService.listenResults("Ok. Navigating to: " + params);
        }
    }

    /**
     * Attempts to open an app based on the params. If no exact match is found, a close match is chosen. And
     * if no close match is chosen, this method does not open anything.
     * @param command the command that called this method
     * @param params the parameters associated with the command
     */
    public static void openApp(String command, String params) {
        if (params.length() == 0)
            mAVRAService.listenResults(command + "? " + command + " what? I can't read your mind!");
        else {
            String trimmedParams = params.replace(" ", "").toLowerCase();
            String [] paramTokens = params.toLowerCase().split(" ");
            PackageManager pm = mAVRAService.getPackageManager();
            if (pm != null && sActivityData != null) {
                boolean match = false;
                int matchId = -1;
                int longestMatch = 0;
                int matches = 0;
                for (int i = 0; sActivityData != null && i < sActivityData.size() && !match; i++) {
                    String label = sActivityData.get(i).toString().replace("'", "").toLowerCase();
                    if (label.replace(" ", "").equals(trimmedParams)) {
                        match = true;
                        matchId = i;
                    }
                    else {
                        int tempLongest = 0;
                        int tempMatches = 0;
                        for (String token : paramTokens) {
                            if (label.contains(token)) {
                                tempMatches++;
                                if (token.length() > tempLongest)
                                    tempLongest = token.length();
                            }
                        }
                        if (tempLongest > longestMatch)
                            if (tempMatches > matches) {
                                longestMatch = tempLongest;
                                matches = tempMatches;
                                matchId = i;
                            }
                    }
                }

                if (matchId > -1) {
                    ActivityInfo activity = sActivityData.get(matchId).getResolveInfo().activityInfo;
                    ComponentName name=new ComponentName(activity.applicationInfo.packageName,
                            activity.name);
                    Intent i=new Intent(Intent.ACTION_MAIN);

                    i.addCategory(Intent.CATEGORY_LAUNCHER);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    i.setComponent(name);
                    mAVRAService.startActivity(i);

                    Drawable icon;
                    try {
                        icon = pm.getActivityIcon(name);
                    } catch (PackageManager.NameNotFoundException e) {
                        icon = null;
                    }

                    HistoryContainer.createHistoryContainer()
                            .setIsResponse(true)
                            .setIcon(icon)
                            .setType(HistoryContainer.TYPE_RESULT)
                            .setText("Opened " + sActivityData.get(matchId).mLabel)
                            .sendToAdapter();

                    if (match)
                        mAVRAService.listenResults("Opening " + sActivityData.get(matchId).mLabel);
                    else
                        mAVRAService.listenResults(sActivityData.get(matchId).mLabel + " was the closest match");

                }
                else
                    mAVRAService.listenResults("Error opening " + params);

            }
            else
                mAVRAService.listenResults("Error opening " + params);
        }

    }

    public static void question(final String command, final String params) {
        if (params.length() > 0) {
            final class QuestionResults {
                public final String description, source, url;
                QuestionResults(String description, String source, String url) {
                    this.description = description;
                    this.source = source;
                    this.url = url;
                }
            }

            mAVRAService.mAVRAState.setProcessing();
            mAVRAService.updateActivityUi();
            new AsyncTask<Void,Void,QuestionResults>() {

                @Override
                protected QuestionResults doInBackground(Void... voids) {
                    try {
                        HttpClient client = new DefaultHttpClient();
                        client.getParams().setParameter(CoreProtocolPNames.USER_AGENT, "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0");
                        HttpGet queryRequest = new HttpGet("https://www.googleapis.com/freebase/v1/search?query=" + params.replace(' ', '+') + "&key=" + GOOGLE_API_KEY);
                        HttpResponse queryResponse = client.execute(queryRequest);
                        JSONObject jsonQueryResponse = new JSONObject(EntityUtils.toString(queryResponse.getEntity()));
                        JSONArray jsonQueryResults = (JSONArray)jsonQueryResponse.get("result");
                        JSONObject mostLikelyResult = jsonQueryResults.getJSONObject(0);
                        String id = mostLikelyResult.getString("id");

                        HttpGet topicRequest = new HttpGet("https://www.googleapis.com/freebase/v1/topic" + id + "?key=" + GOOGLE_API_KEY);
                        HttpResponse topicResponse = client.execute(topicRequest);
                        JSONObject jsonTopicResponse = new JSONObject((EntityUtils.toString(topicResponse.getEntity())));
                        JSONObject jsonTopicResult = jsonTopicResponse.getJSONObject("property");

                        JSONObject topicDescription = jsonTopicResult.getJSONObject("/common/topic/description");
                        JSONArray descriptionArray = topicDescription.getJSONArray("values");
                        String description = null, source = null, url = null;
                        for (int i = 0; i < descriptionArray.length(); i++) {
                            try {
                                JSONObject descriptionObject = descriptionArray.getJSONObject(i);
                                description = descriptionObject.getString("value");
                                JSONObject citation = descriptionObject.getJSONObject("citation");
                                source = citation.getString("provider");
                                url = citation.getString("uri");
                                i = descriptionArray.length();
                            } catch (JSONException e) {
                                description = source = url = null;
                            }
                        }
                        return new QuestionResults(description, source, url);


                    } catch (Exception e) {
                        e.printStackTrace();
                        return null;
                    }
                }

                @Override
                protected void onPostExecute(QuestionResults questionResults) {
                    mAVRAService.mAVRAState.setNotProcessing();
                    mAVRAService.updateActivityUi();
                    if (questionResults != null && questionResults.description != null) {
                        String result = questionResults.description;
                        int period = result.indexOf(". ", 60);  //find location of first period ". " after 60
                        if (period < 1)
                            period = AVRASettings.HISTORY_CONTAINER_MAX_TEXT_LENGTH;
                        result = result.substring(0, Math.min(period, Math.min(AVRASettings.HISTORY_CONTAINER_MAX_TEXT_LENGTH, result.length())));
                        if (result.length() == AVRASettings.HISTORY_CONTAINER_MAX_TEXT_LENGTH)
                            result += "...";
                        result = "According to " + questionResults.source + ", " + result;
                        HistoryContainer.createHistoryContainer()
                                .setType(HistoryContainer.TYPE_RESULT)
                                .setIsResponse(true)
                                .setText("Found result for: " + params + ". Source: " + questionResults.source + ". URL: " + questionResults.url)
                                .setIconId(android.R.drawable.ic_menu_info_details)
                                .sendToAdapter();
                        mAVRAService.listenResults(result);
                    }
                    else
                        mAVRAService.listenResults("Sorry. I could not find anything.");
                }
            }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
        }
        else
            mAVRAService.listenResults("That's pretty vague...");

    }

    /**
     * Repeats what is specified in the params.
     * @param command the command that called this method
     * @param params the parameters associated with the command
     */
    public static void repeat(String command, String params) {
        if (params.equals(""))
            mAVRAService.listenResults("Here's an idea! Maybe next time you should tell me something to say!");
        else
            mAVRAService.listenResults(params);
    }

    /**
     * Translate the shortcut phrase into a command with parameters. Uses {@link com.kflis.test.driverbuddy.CommandList.AVRAShortcut#sShortcutMap}
     * to retrieve the {@link com.kflis.test.driverbuddy.CommandList.AVRAShortcut} associated with the phrase. Then begins the command based on the contents
     * of the {@link com.kflis.test.driverbuddy.CommandList.AVRAShortcut}
     * @param command the shortcut being used
     * @param params if a shortcut has params, it is not a valid shortcut
     */
    public static void shortcut(String command, String params) {
        if (sShortcutMap != null && params.length() == 0) {
            AVRAShortcut info = sShortcutMap.get(command);
            if (info != null) {
                String shortcutParams = info.getParams();
                AVRACommand avraCommand = info.getCommand();
                if (shortcutParams != null && avraCommand != null) {
                    avraCommand.begin(command, shortcutParams);
                    return;
                }
            }
        }
        mAVRAService.listenResults("Error");
    }

    public static void startWordActivator(String command, String params) {
        if (mAVRAService.mAVRAState.isActivatorOn())
            mAVRAService.listenResults("Word activation is already on, silly.");
        else {
            mAVRAService.mAVRAState.setActivator();
            mAVRAService.listenResults("Ok. Say " + AVRASettings.WAKE_UP_PHRASE + " to activate me!");
        }
    }

    public static void stopWordActivator(String command, String params) {
        if (!mAVRAService.mAVRAState.isActivatorOn())
            mAVRAService.listenResults("Word activation is not on, dummy.");
        else {
            mAVRAService.mAVRAState.setNotActivator();
            mAVRAService.listenResults("Ok. Word activation is disabled.");
        }
    }

    public static void wifiOff(String command, String params) {
        WifiManager wifi = (WifiManager) mAVRAService.getSystemService(Context.WIFI_SERVICE);
        if (wifi.getWifiState() == WifiManager.WIFI_STATE_DISABLED) {
            mAVRAService.listenResults("Sure, I'll turn off the already disabled wifi.");
        }
        else {
            wifi.setWifiEnabled(false);
            mAVRAService.listenResults("Wifi has been disabled.");
        }
    }

    public static void wifiOn(String command, String params) {
        WifiManager wifi = (WifiManager) mAVRAService.getSystemService(Context.WIFI_SERVICE);
        if (wifi.getWifiState() == WifiManager.WIFI_STATE_ENABLED) {
            mAVRAService.listenResults("Wifi is on, so I'll increase the signal strength by over 9000 decibels!");
        }
        else {
            wifi.setWifiEnabled(true);
            mAVRAService.listenResults("Wifi has been enabled.");
        }
    }

    /**
     * Helper class for storing names applications located in the launcher, along with their
     * respective {@link android.content.pm.ResolveInfo}
     */
    public static final class ActivityData {
        private final String mLabel;
        private final ResolveInfo mResolveInfo;

        public ActivityData(String label, ResolveInfo resolveInfo) {
            this.mLabel = label;
            this.mResolveInfo = resolveInfo;
        }

        public ResolveInfo getResolveInfo() { return mResolveInfo; }

        @Override
        public String toString() {
            return mLabel;
        }

        @Override
        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (o == this)
                return true;
            return o.toString().equals(this.toString()) && o instanceof ActivityData;
        }
    }

    /**
     * Container class to store a shortcut command along with the parameters that should be used
     * when the shortcut command is called.
     */
    public static final class AVRAShortcut {
        private final AVRACommand mAVRACommand;
        private final String mParams;

        public AVRAShortcut(AVRACommand avraCommand, String params) {
            this.mAVRACommand = avraCommand;
            this.mParams = params;
        }

        public AVRACommand getCommand() { return mAVRACommand; }

        public String getParams() { return mParams; }
    }
}
