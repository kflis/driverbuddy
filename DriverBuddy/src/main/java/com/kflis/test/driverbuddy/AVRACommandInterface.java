package com.kflis.test.driverbuddy;

/**
 * Simple command interface that specifies a command and its parameters.
 */
public interface AVRACommandInterface {
    public void begin(String command, String params);
}
