package com.kflis.test.driverbuddy;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

/**
 * This class does any necessary preloading upon activity creation. After creating a new instance of AVRAPreload,
 * {@link AVRAPreload#getStatus()} will return false until complete.
 */
public class AVRAPreload {
    private boolean preloadFinished;
    private int perc = 0;
    private int resource = 0;
    private final AVRAService avraService;

    public AVRAPreload (AVRAService avraService) {
        this.avraService = avraService;
        preload();
    }

    private void preload() {

        new AsyncTask<Void,Void,Void>() {
            @Override
            protected Void doInBackground(Void... voids) {

                //cache names of launcher activity names to CommandList.sActivityData to quickly open applications using the CommandList.OPEN_APP command
                if (CommandList.sActivityData == null) {
                    resource = R.string.populating_app_list;
                    PackageManager pm = avraService.getPackageManager();
                    if (pm != null) {
                        Intent main = new Intent(Intent.ACTION_MAIN, null);

                        main.addCategory(Intent.CATEGORY_LAUNCHER);

                        List<ResolveInfo> activities = pm.queryIntentActivities(main, 0);
                        ArrayList<CommandList.ActivityData> names = new ArrayList<CommandList.ActivityData>();
                        //CharSequence charSequence;
                        int index = 0;
                        for (ResolveInfo resolveInfo : activities) {
                            if (resolveInfo != null) {
                                ActivityInfo activityInfo = resolveInfo.activityInfo;
                                if (activityInfo != null) {
                                    CharSequence charSequence = activityInfo.loadLabel(pm);
                                    if (charSequence != null)
                                        names.add(new CommandList.ActivityData(charSequence.toString(), resolveInfo));
                                }
                            }
                            perc = (++index * 100) / activities.size();

                        }
                        CommandList.sActivityData = names;
                    }
                }

                //first add shortcuts to CommandList.sShortcutMap
                CommandList.sShortcutMap.put("open my favorite app", new CommandList.AVRAShortcut(CommandList.getCommand(CommandList.OPEN_APP), "reddit news"));

                //then add the commands using avraService.loadCommands(ArrayList of CommandAssociation), including shortcut commands
                ArrayList<AVRACommandDispatcher.CommandAssociation> commands = new ArrayList<AVRACommandDispatcher.CommandAssociation>();
                commands.add(new AVRACommandDispatcher.CommandAssociation("open", CommandList.getCommand(CommandList.OPEN_APP)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("launch", CommandList.getCommand(CommandList.OPEN_APP)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("say", CommandList.getCommand(CommandList.REPEAT)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("please say", CommandList.getCommand(CommandList.REPEAT)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("repeat after me", CommandList.getCommand(CommandList.REPEAT)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("repeat", CommandList.getCommand(CommandList.REPEAT)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("enable word activation", CommandList.getCommand(CommandList.START_WORD_ACTIVATOR)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("disable word activation", CommandList.getCommand(CommandList.STOP_WORD_ACTIVATOR)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("navigate to", CommandList.getCommand(CommandList.NAVIGATE)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("disable wifi", CommandList.getCommand(CommandList.WIFI_OFF)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("turn off wifi", CommandList.getCommand(CommandList.WIFI_OFF)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("turn wifi off", CommandList.getCommand(CommandList.WIFI_OFF)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("enable wifi", CommandList.getCommand(CommandList.WIFI_ON)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("turn on wifi", CommandList.getCommand(CommandList.WIFI_ON)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("turn wifi on", CommandList.getCommand(CommandList.WIFI_ON)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("open my favorite app", CommandList.getCommand(CommandList.SHORTCUT)));
                //commands.add(new AVRACommandDispatcher.CommandAssociation("how", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what is", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what is the", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what is an", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what is a", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what's", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what's the", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what's a", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("what's an", CommandList.getCommand(CommandList.QUESTION)));
                //commands.add(new AVRACommandDispatcher.CommandAssociation("why", CommandList.getCommand(CommandList.QUESTION)));
                //commands.add(new AVRACommandDispatcher.CommandAssociation("is", CommandList.getCommand(CommandList.QUESTION)));
                //commands.add(new AVRACommandDispatcher.CommandAssociation("did", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("who", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("who is", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("who is the", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("who's", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("who's the", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("where", CommandList.getCommand(CommandList.QUESTION)));
                commands.add(new AVRACommandDispatcher.CommandAssociation("where is", CommandList.getCommand(CommandList.QUESTION)));
                avraService.loadCommands(commands);


                //wait for AVRATextToSpeech to initialize
                while (!avraService.isTTSInitialized()) {
                    resource = R.string.loading_tts;
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        Thread.currentThread().interrupt();
                    }
                }

                preloadFinished = true;
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (avraService.mAVRAState.isActivatorOn())
                    avraService.startWordActivator();
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, null, null, null);
    }

    public PreloadStatus getStatus() {
        return new PreloadStatus(perc, resource, preloadFinished);
    }

    /**
     * Helpful container that contains information about preloading being finished,
     * the resource id of a string to be displayed, and percentage complete. Used to update
     * {@link com.kflis.test.driverbuddy.PreloadFragment} with the latest information.
     */
    public class PreloadStatus {
        public int perc;
        public int resource;
        public boolean done;

        public PreloadStatus(int perc, int resource, boolean done) {
            this.perc = perc;
            this.resource = resource;
            this.done = done;
        }
    }
}
