package com.kflis.test.driverbuddy;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * This class is used to place and retrieve commands in a multi-level {@link java.util.HashMap}
 */
public class AVRACommandDispatcher {

    HashMap<String, HashHolder> rootMap;
    AVRAService avraService;

    public AVRACommandDispatcher(AVRAService avraService) {
        this.avraService = avraService;
       rootMap = new HashMap<String, HashHolder>(AVRASettings.ROOT_HASH_INITIAL_SIZE, AVRASettings.ROOT_HASH_LOAD_FACTOR);

    }

    /**
     * Parses a list of commands and executes the first valid command in the deepest scope of the tree. <p></p>
     *
     * This algorithm allows for a command with parameters (say "open") to be overridden by another command (say "open favorite app") depending on the user's customizations.
     * In addition, commands without parameters are only recognized if the command being parsed does not have parameters. (i.e. if "open favorite app" was a command without parameters,
     * "open favorite app" would go to the command/shortcut associated with "open favorite app," but "open favorite app now" would be just be associated with the "open" command).
     * And if a command with parameters overrides another command with parameters (say "open website" overrides "open"), every token after "open website" is associated with the
     * "open website" command, rather than the "open" command.
     * @param commandList list of commands to be parsed.
     */
    public void dispatch(ArrayList<String> commandList) {
        boolean match = false;
        for (int x = 0; x < commandList.size() && !match; x++) { //for every command in the commandList
            String command = commandList.get(x); //get the current command
            String [] tokens = command.split(" "); //split it up into tokens
            HashMap<String, HashHolder> hashScope = rootMap; //start off by searching the rootMap
            AVRACommand avraCommand = null;
            int paramIndex = 0; //index of where parameters start in tokens[]
            for (int i = 0; i < tokens.length && hashScope != null; i++) { //for every token
                HashHolder hashHolder = hashScope.get(tokens[i]); //get the HashHolder from the current hashScope
                if (hashHolder != null) { //if the hashScope exists
                    String s = concat(tokens, 0, i); //concatenate the tokens from 0 to i to create a potential command
                    CommandAssociation commandAssociation = hashHolder.checkIfSuccess(s);
                    if (commandAssociation != null) { //if the tokens from 0 to i are associated with a command in the current hashScope
                        if (!(!commandAssociation.getCommand().hasParams() && (i+1) != tokens.length)) { //always accept commands that are followed by parameters
                            avraCommand = commandAssociation.getCommand();                               //and if the command found in the hashScope has no parameters, make sure the command being parsed also has no more tokens left to parse
                            paramIndex = i + 1; //set the starting location of the parameters in the tokens array
                        }
                    }
                    hashScope = hashHolder.getMap(); //check the next level of the HashMap to see if any other commands take priority
                }
                else //if the HashHolder does not exist, we are finished searching the HashMaps for the current command
                    i = tokens.length;
            }
            if (avraCommand != null) {
                match = true;
                HistoryContainer.createHistoryContainer()
                        .setIsResponse(false)
                        .setIconId(android.R.drawable.stat_notify_chat)
                        .setType(HistoryContainer.TYPE_COMMAND)
                        .setText("\"" + command + "\"")
                        .sendToAdapter();
                avraCommand.begin(concat(tokens, 0, paramIndex-1), concat(tokens, paramIndex, tokens.length-1));
            }
        }

        if (!match) {
            HistoryContainer.createHistoryContainer()
                    .setIsResponse(false)
                    .setIconId(android.R.drawable.stat_notify_chat)
                    .setType(HistoryContainer.TYPE_COMMAND)
                    .setText("\"" + commandList.get(0) + "\"")
                    .sendToAdapter();
            avraService.listenResults("Command not found");
        }
    }

    /**
     * Concatenate parts of a {@link java.lang.String}[] into a single {@link java.lang.String}
     * @param strings the {@link java.lang.String} array
     * @param startPos the starting position in the array
     * @param endPos the end position in the array
     * @return the concatenation of the specified {@link java.lang.String}s
     */
    public String concat(String[] strings, int startPos, int endPos) {

        String temp = "";
        int terminate = Math.min(endPos,strings.length-1);
        for (int i = startPos; i <= terminate; i++)
            temp += strings[i] + " ";
        temp = temp.trim();
        return temp;
    }

    /**
     * For every command mapping, add the activation string into the hash table.
     * @param arrayList list of command mappings to be put into the hash table
     */
    public void populate(ArrayList<CommandAssociation> arrayList) {
        for (CommandAssociation commandAssociation : arrayList) {
            String [] tokens = commandAssociation.getCommandString().split(" ");
            HashMap<String, HashHolder> hashScope = rootMap; //start with the first level of mappings
            for (int i = 0; i < tokens.length; i++) {
                HashHolder hashHolder = hashScope.get(tokens[i]); //get the HashHolder associated with current token in the current hash table scope
                if (hashHolder == null) { //if one doesn't exist, create it
                    hashHolder = new HashHolder();
                }
                hashScope.put(tokens[i], hashHolder); //put the "i"th token of the command string in the current hash table scope
                if ((tokens.length -1) == i)
                    hashHolder.addSuccessfulCommand(commandAssociation); //if this is the last token of the command, then we know it signifies a successful command,
                                                                 //so add it to the current hashmap's hashholder's list of successful commands at this bucket

                else {                                          //otherwise, map the token in the current hashScope to the next level
                    hashScope = hashHolder.getAndMaybeCreateMap();
                }

            }

        }

    }


    /**
     * Helpful container used to hold to child {@link java.util.HashMap}s and successful commands associated with a key in a given HashMap scope.
     */
    private static class HashHolder {
        private HashMap<String, HashHolder> hashMap = null;
        private ArrayList<CommandAssociation> successfulCommands;


        public HashMap<String, HashHolder> getAndMaybeCreateMap() {
            if (hashMap == null)
                hashMap = new HashMap<String, HashHolder>(AVRASettings.CHILD_HASH_INITIAL_SIZE, AVRASettings.CHILD_HASH_LOAD_FACTOR);
            return hashMap;
        }

        public HashMap<String, HashHolder> getMap() {
            return hashMap;
        }


        public void addSuccessfulCommand(CommandAssociation successfulMap) {
            if (successfulCommands == null)
                successfulCommands = new ArrayList<CommandAssociation>();
            successfulCommands.add(successfulMap);
        }

        public CommandAssociation checkIfSuccess(String command) {
            if (successfulCommands != null)
                for (CommandAssociation commandAssociation : successfulCommands)
                    if (commandAssociation.getCommandString().equals(command))
                        return commandAssociation;
            return null;
        }

    }

    /**
     * Container to associate strings with an AVRACommand.
     */
    public final static class CommandAssociation {
        private final String commandString;
        private final AVRACommand avraCommand;

        public CommandAssociation(String commandString, AVRACommand avraCommand) {
            this.commandString = commandString.toLowerCase().trim();
            this.avraCommand = avraCommand;
        }

        public String getCommandString() { return commandString; }

        public AVRACommand getCommand() {return avraCommand; }
    }

}
