package com.kflis.test.driverbuddy;

import android.graphics.drawable.Drawable;
import android.text.format.DateFormat;

import java.util.Date;

/**
 * Class for use with {@link com.kflis.test.driverbuddy.HistoryListAdapter} to hold the data to be displayed.
 * Always use use {@link HistoryContainer#createHistoryContainer()} to create a new instance of a history container.
 * Then after all the data has been set, use {@link HistoryContainer#sendToAdapter()} to register it with the adapter.
 */
public class HistoryContainer {

    public static final String TYPE_ACTION = "Action";
    public static final String TYPE_RESPONSE = "Response";
    public static final String TYPE_COMMAND = "Command";
    public static final String TYPE_RESULT = "Result";

    private int mIconId = 0;
    private Drawable mDrawable = null;
    private String mType;
    private String mTime;
    private String mText;
    private boolean mIsResponse;
    private static HistoryListAdapter mAdapter;

    public static HistoryContainer createHistoryContainer() {
        return new HistoryContainer();
    }

    private HistoryContainer () {
        this.mIconId = 0;
        this.mType = "";
        this.mText = "";
        this.mIsResponse = true;
        mTime = new DateFormat().format("hh:mm", new Date()).toString();
    }

    /**
     * This method MUST be called before this class is used, otherwise a {@link java.lang.NullPointerException}
     * will be thrown while trying to add an entry to a null {@link com.kflis.test.driverbuddy.HistoryListAdapter}
     * @param adapter the {@link com.kflis.test.driverbuddy.HistoryListAdapter} that will be used
     */
    public static void setAdapter(HistoryListAdapter adapter) { mAdapter = adapter; }

    public HistoryContainer setIconId(int resource) { this.mIconId = resource; return this; }

    public HistoryContainer setIcon(Drawable img) { this.mDrawable = img; return this; }

    public HistoryContainer setType(String type) { this.mType = type; return this; }

    public HistoryContainer setText(String text) { this.mText = text; return this; }

    public HistoryContainer setIsResponse(boolean isResponse) { this.mIsResponse = isResponse; return this; }

    public int getIconId() { return mIconId; }

    public Drawable getIcon() { return mDrawable; }

    public String getType() { return mType; }

    public String getTime() { return mTime; }

    public String getText() { return mText; }

    public boolean isResponse() { return mIsResponse; }

    public void sendToAdapter() { mAdapter.add(this); }
}
