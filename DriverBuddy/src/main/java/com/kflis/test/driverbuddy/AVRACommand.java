package com.kflis.test.driverbuddy;

/**
 * The class used to create new commands.
 */
public abstract class AVRACommand implements AVRACommandInterface {
    private final boolean usesParams;

    AVRACommand(boolean usesParams) {
        this.usesParams = usesParams;
    }

    public boolean hasParams() { return usesParams; }
}
